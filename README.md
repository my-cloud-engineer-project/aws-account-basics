# AWS Account Basics
### AWS Account 101


- Create an IAM user for your personal use.

- Set up MFA for your root user, turn off all root user API keys.

- Set up Billing Alerts for anything over a few dollars.

- Configure the AWS CLI for your user using API credentials.

- Checkpoint: You can use the AWS CLI to interrogate information about your AWS account.

***

# CLI Examples 

``` 
aws iam get-account-summary
```
- Returns everything you could think of for the account
```
aws iam get-account-password-policy
```
-    Returns specific details about the set password policy

```
aws s3 ls
```
- Returns a list of all the buckets you have in aws along with their creation dates

```
aws route53 list-hosted-zones-by-name
```
- returns my hosted zones id as well as the name and other details associated to hosted zone


---

![alt text][logo]

[logo]: https://d1yjjnpx0p53s8.cloudfront.net/styles/logo-thumbnail/s3/102017/logo_0.png?17TK91b1B6OvV2MFrCLfukw1c8oEaNr6&itok=vsanFiUj "AWS Logo"